

<h2>Forum app</h3>
<a target="_blank" href="/forum">Main Page</a>
<p>Powered by <a href="https://github.com/ellmetha/django-machina">django-machina forum app</a></p>
<p>Translated this django app and coverted it's templates to rtl</p>

<p>Used django.contrib.auth and <a href="https://django-oauth-toolkit.readthedocs.io/en/latest/">Django OAuth Toolkit</a> for implementing an OAuth2 Authentication process</p>

<hr>
<h2>Rest Api</h3>
<p>Used <a href="https://www.django-rest-framework.org/">Django Rest Framework</a> for it's restful api implementation</p>
<p>clone the project from it's <a target="_blank" href="https://gitlab.com/mohsenbarzegar1/autofunion/-/tree/development">Source Code</a>, run locally and test the authentication api</p>

<hr>
<h3>Board endpoints:</h3>
<div class="api">
<h4>Board messages and tags ( method: GET )</h4>
<a target="_blank" href="http://autofunion.pythonanywhere.com/api/v1/board/">/api/v1/board/</a>
<br>
<a target="_blank" href="http://autofunion.pythonanywhere.com/api/v1/tags/">/api/v1/tags/</a>
<br>
<a target="_blank" href="http://autofunion.pythonanywhere.com/api/v1/tags/4/messages/">/api/v1/tags/{tagId}/messages/</a>
</div>

<hr>
<div class="api">
<h4>Send new message to baord ( method: POST )</h4>
<a target="_blank" href="http://autofunion.pythonanywhere.com/api/v1/board/">/api/v1/board/</a>
<p>header: 'Authorization': 'Bearer [token]'</p>
<p>request body: { "text": "some sample text", "tags": [ { "id": "first_tag_id" }, { "id": "second_tag_id" } ] }</p>
</div>

<h4>User Authentication endpoints</h3>
<p>( sample registered user: <span>username: "folane" password: folfol1234</span> )</p>

<hr>
<div class="api">
<p>Invite a user by admin ( method: POST )</p>
<p>request body: {"id": "930013009"}</p>
<a href="">/authentication/invite/</a>
</div>

<hr>
<div class="api">
<p>Get user OAuth2 token for using the api ( method: POST )</p>
<p>request body: {"username": USERNAME, "password": PASSWORD}</p>
<a href="">/authentication/token/</a>
<hr>
</div>

<hr>
<div class="api">
<p>Get student id with invite token ( method: POST )</p>
<p>request body: {"invite_token": "invite_token"}</p>
<a href="">/authentication/invite/who/</a>
</div>

<hr>
<div class="api">
<p>Revoke a user's access token ( method: POST )</p>
<p>request body: {"token": "token"}</p>
<a href="">/authentication/token/revoke/</a>
</div>

<hr>
<div class="api">
<p>Registers user to the server ( method: POST )</p>
<p>request body: {"refresh_token": "refresh_token"}</p>
<a href="">/authentication/token/refresh/</a>
</div>

</body>
</html>

